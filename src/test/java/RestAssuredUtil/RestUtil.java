package RestAssuredUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import org.json.JSONObject;
import io.restassured.RestAssured;
import RestAssuredConfig.Restassuredconfigfile;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestUtil {
	

public static String readPropertyFile(String key)
{ 

String value = "";
try{            
Properties prop = new Properties();
File f = new File(System.getProperty("user.dir")+"\\src\\test\\java\\RestAssuredUtil\\propertyfile.properties");
if(f.exists()){
prop.load(new FileInputStream(f));
value = prop.getProperty(key); 
}
}
catch(Exception e){  
System.out.println("Failed to read from Properties file.");  
}
return value;
} 

public static   void logintoken() throws Exception {

String str = RestUtil.readPropertyFile("username");
System.out.println(str);
StringBuilder sb = new StringBuilder("grant_type=password&client_id=daecdcd831e34800f061af1918f03dae&password=strange!&username=");
String myJson = sb.insert(sb.toString().length(), str).toString();
System.out.println(sb);
RestAssured.baseURI = "https://stage.upwork.com/api/v3/oauth2/token";
RequestSpecification httpRequest = RestAssured.given();
Response r = httpRequest.body(myJson).contentType("application/x-www-form-urlencoded").post();
System.out.println(r.asString());
System.out.println(r.getStatusCode());


//Convert response body from String to key : value pair
/*Map<Object,Object> responsebody = new HashMap<Object, Object>();
JSONObject jObject = new JSONObject(r.getBody().asString());
Iterator<?> keys = jObject.keys();
String accessToken = "";
while (keys.hasNext()) {
String key = (String) keys.next();
if (key.equals("access_token")) {
accessToken = jObject.getString(key);
		
//Print access token
System.out.println(accessToken);

}
}
return accessToken;*/		
}	

	
public static void main(String args[]) throws Exception{  
RestUtil obj1 = new RestUtil();
obj1.logintoken();
	
}

}
