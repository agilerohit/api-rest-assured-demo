package RestAssuredConfig;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.BeforeSuite;
import RestAssuredValidation.RestAssuredvalidation;
import RestAssuredHelper.HelperMethods;
import RestAssuredUtil.RestUtil;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class Restassuredconfigfile extends HelperMethods{
	
// Reference varaible
protected RestUtil restUtil = new RestUtil();
protected HelperMethods helperMethods = new HelperMethods();
protected RestAssuredvalidation restAssuredvalidation = new RestAssuredvalidation();


@BeforeSuite(alwaysRun=true)

public void configure()
{
RestAssured.baseURI= "https://stage.upwork.com";
}

public RequestSpecification getRequestSpecification()

{
return RestAssured.given();
	
}

public Response getResponse(RequestSpecification requestSpecification, String endpoint)

{
Response response = requestSpecification.get(endpoint);
response.then().log().all();
return response;
}

//This method generate token
public  String authToken(String myJson) throws JSONException
{
		
RequestSpecification httpRequest = RestAssured.given();
Response r = httpRequest.body(myJson).when().contentType("application/x-www-form-urlencoded").post("https://stage.upwork.com/api/v3/oauth2/token");
		
//Convert response body from String to key : value pair
Map<Object,Object> responsebody = new HashMap<Object, Object>();
JSONObject jObject = new JSONObject(r.getBody().asString());
Iterator<?> keys = jObject.keys();
String accessToken = "";
while (keys.hasNext()) {
String key = (String) keys.next();
if (key.equals("access_token")) {
accessToken = jObject.getString(key);
		
//Print access token
System.out.println(accessToken);
		
}
}
return accessToken;
}



}