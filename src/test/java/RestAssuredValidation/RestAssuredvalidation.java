package RestAssuredValidation;

import org.testng.Assert;

import RestAssuredHelper.HelperMethods;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class RestAssuredvalidation extends HelperMethods{

// Assert that correct status code is returned.	
public void validateResponseCode(Response res)	
{
int statusCode = res.getStatusCode();
Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
}

// Validate response header
public void validateResponseHeader(Response res)
{
String contentType = res.header("Content-Type");
Assert.assertEquals(contentType /* actual value */, "application/json" /* expected value */);

String serverType =  res.header("Server");
Assert.assertEquals(serverType /* actual value */, "nginx" /* expected value */);

String contentval = res.header("Connection");
Assert.assertEquals(contentval, "keep-alive");
}

// Validate response body
public void validateResponseBody(Response res)
{
// First get the JsonPath object instance from the Response interface
JsonPath jsonPathEvaluator = res.jsonPath();
String id = jsonPathEvaluator.get("metadata");
// Validate the response
//Assert.assertEquals(id, "929045658099318784", "Verify Id in the Response");
}

}