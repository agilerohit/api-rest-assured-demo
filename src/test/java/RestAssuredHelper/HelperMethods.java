package RestAssuredHelper;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static org.junit.Assert.assertEquals;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import org.json.JSONException;
import org.json.JSONObject;
import RestAssuredConfig.Restassuredconfigfile;



public class HelperMethods {
	

	//End Points
	public interface EndPoint {
	String getOauthtoken ="/api/v3/oauth2/token";	
	String getats ="/api/v3/mobile/jobs/postings";
	String joburl="/api/v3/mobile/jobs/930069711882878976/applications";
	}

	     
    //Common request headers
    public LinkedHashMap<String, String> addHeader(){
    LinkedHashMap<String, String> headers =new LinkedHashMap<String, String>();
	headers.put("Cache-Control", "no-cache");
	headers.put("Accept", "application/json");
	headers.put("X-Upwork-Mobile-Network", "3G");
	headers.put("X-Upwork-Mobile-Platform", "ios");
	headers.put("X-Upwork-Mobile-Language", "en");
	headers.put("X-Upwork-Mobile-Version", "1.2.3");
	headers.put("Content-Type", "application/json");
	return headers;
	}
	
	
	//This method generate response body
	public static void getResponseBody (Response res) {
    System.out.println(res.asString());
	}
	
	//This method generate response header
	public static void getResponseheader (RequestSpecification httpRequest,Response response) {
	for(Header hd:response.getHeaders())
	{
	System.out.println("key:" +hd.getName() + "Value" +hd.getValue());
	}
		
	}
	
	//This method generate status code
	public void getStatusCode(Response res)
	{
	res.getStatusCode();
	System.out.println(res.getStatusCode());
	}
	
	//This method generate status line
	public void getStatusLine(Response res)
	{
	res.getStatusLine();
	System.out.println(res.getStatusLine());
	}
	
	}

    


