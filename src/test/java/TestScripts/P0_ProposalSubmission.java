package TestScripts;

import java.io.IOException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import RestAssuredConfig.Restassuredconfigfile;
import RestAssuredHelper.HelperMethods;
import RestAssuredUtil.RestUtil;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class P0_ProposalSubmission extends Restassuredconfigfile{
	
	
	String token;
	String methodbody="coverLetter:test cover&estimatedDuration:1&proposedAmount:200.0";
	@BeforeMethod
	public void gettoken() throws Exception
	
	{
	//token= authToken("grant_type=password&client_id=daecdcd831e34800f061af1918f03dae&username=rohitfreelancer&password=strange!");
	token= authToken("grant_type=password&client_id=daecdcd831e34800f061af1918f03dae&username=rohitfreelancer&password=strange!");	
	}
	
	@Test()
	public void test() throws Exception
	{
	String val=restUtil.readPropertyFile("X-Upwork-Mobile-Org");	
	System.out.println(val);
	String authorizationval = "bearer " + token;
	RequestSpecification httpRequest= getRequestSpecification();
	Response res = getResponse(httpRequest.headers(addHeader()).headers("Authorization", authorizationval).headers("X-Upwork-Mobile-OrgUid",val).body(methodbody),EndPoint.joburl);
	HelperMethods.getResponseBody(res);
	//HelperMethods.getResponseheader(httpRequest, res);
	//helperMethods.getStatusCode(res);
	//helperMethods.getStatusLine(res);
	//restAssuredvalidation.validateResponseCode(res);
	//restAssuredvalidation.validateResponseHeader(res);
	}
}
